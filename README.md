
# Perception-and-Energy-aware Motion Planning for UAV using Learning-based Model under Heteroscedastic Uncertainty
 (ICRA 2024 submitted)

This is a repository for the following paper:

- Reiya Takemura, Genya Ishigami, "Perception-and-Energy-aware Motion Planning for UAV using Learning-based Model under Heteroscedastic Uncertainty," IEEE ICRA, 2024 submitted. [paper] 

![](doc/framework.png)

We propose perception-and-energy-aware motion planning for UAVs under heteroscedastic uncertainty assumption, enabling them to fly while maintaining energy efficiency and pose estimation accuracy. Our key idea is to model heteroscedastic uncertainty in LiDAR measurements correlated with the velocity of a UAV. The learned model is used to estimate perception quality of the LiDAR-based SLAM. Then, the RRT*-based planner optimizes the perception quality while maintaining the energy efficiency.

![](doc/demo_plan.gif)

## Overview

- This repository provides a MATLAB code to test our algorithm.
- `Octave` is possibly another tool to try our code.

## Getting started
The code has been tested on MATLAB 2020b with the 2 GHz CPU and 16 GB RAM.

### Create raw map 
- Please prepare raw map for trajectory planning simulation like below.
- With Powerpoint, it is easy.
![](doc/map1.png)

### Show map with perception quality (FIM value)
- You can convert a raw map to the map with FIM value.
- The following figures are the maps with velocities [1.0, 4.0, 7.0, 10.0] m/s, respectively.
![](doc/maps_fim_value.png)


### Run trajectory planning 
- Go to `main_plan.m` to run example demonstration of trajectory planning.
- The following figure shows the planning results: i) energy-aware, ii) perception-and-energy-aware, and iii) perception-aware trajectories.
![](doc/plan_result_map1.png)

- Velocity profile is also shown.
![](doc/vel_profile.png)


## Citation
```
@misc{takemura2023perceptionandenergyaware,
      title={Perception-and-Energy-aware Motion Planning for UAV using Learning-based Model under Heteroscedastic Uncertainty}, 
      author={Reiya Takemura and Genya Ishigami},
      year={2023},
      eprint={2309.14272},
      archivePrefix={arXiv},
      primaryClass={cs.RO}
}
```
