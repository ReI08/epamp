%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Map.m 
%%% for global map representation 
%%%
%%% 2.10.2023 First edit: R.Takemura
%%% 9.19.2023 Last edit: R.Takemura
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef Map < handle
    
    properties
        map_x
        map_y

        map        
        uav_spec
        
        ax_map
    end
    
    properties (Constant)
        resolution = 1; % map should be given as 250 pixel
    end
    
    methods
        function obj = Map(map_x, map_y, file_path, uav_spec)
            obj.map_x = map_x;
            obj.map_y = map_y;
            obj.uav_spec = uav_spec;
            
            image = obj.load_image(file_path);
            obj.map = obj.gen_map(image);
            
        end
        
        function bwimage = load_image(obj, file_path)
            image = imread(file_path);
            grayimage = rgb2gray(image);
            bwimage = grayimage < 0.5;
        end
        
        function map = gen_map(obj, image)
            figure(1)
            map = binaryOccupancyMap(image, obj.resolution);
            show(map);  
            xlim([0 obj.map_x]);
            ylim([0 obj.map_y]);
        end
        
        function gen_pq_map(obj, map)
                        
            % slow
            x = linspace(0, obj.map_x, obj.map_x*obj.resolution);
            y = linspace(0, obj.map_y, obj.map_y*obj.resolution);
            % compromise
%             x = linspace(0, obj.map_x, 50);
%             y = linspace(0, obj.map_y, 50);
            [X,Y] = meshgrid(x,y);
            Z = zeros(length(y), length(x));
            
            obj.uav_spec.gen_cgg();
            [p_x p_y] = obj.uav_spec.get_fp(map, x, y);

            for i=1:1:length(x)
                for j=1:1:length(y)
                    % too long
                    %[p_x p_y] = obj.uav_spec.lidar_sim(map, x(i), y(j));
                    
                    [grid_x grid_y] = obj.uav_spec.check_grid(p_x - x(i), p_y - y(j));
                    Z(j,i) = obj.uav_spec.calc_fim(grid_x, grid_y, 5);
                end
            end
            
            figure(2)            
            image(x,y,Z, 'CDataMapping','scaled');
            %mesh(X,Y,Z);
            %view(2)
            xlabel('X [meters]')
            ylabel('Y [meters]')
            xlim([0 obj.map_x]);
            ylim([0 obj.map_y]);
            set(gca,'YDir','normal');
            colormap cool
            colorbar;
            pbaspect([1 1 1])
            
            figure(3)            
            ax1 = axes;
            obj.ax_map = ax1;
            image(ax1, x,y,Z, 'CDataMapping','scaled');
            axis equal
            xlabel('X [meters]')
            ylabel('Y [meters]')
            xlim([0 obj.map_x]);
            ylim([0 obj.map_y]);
            set(gca,'YDir','normal');
            colormap(ax1,'cool')
            cb1 = colorbar(ax1,'Position',[0.85 0.1 0.05 0.815]);
            pbaspect([1 1 1])
            caxis([0 350]);
            
            disp("Max eigen of FIM = "+max(max(Z)))
        end
    end
end