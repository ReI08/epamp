%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% RRTStarClass.m 
%%% for RRT* planning algorithm with spline interpolation
%%%  
%%% 2.10.2023 First edit: R.Takemura
%%% 9.19.2023 Last edit: R.Takemura
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


classdef RRTStarClass < handle
    properties
        map
        uav_spec
        
        edge
        xg
        yg
        mid_x
        mid_y
        dist_2g
        
        tree_plot 
        traj_plot 
        traj_opt_x 
        traj_opt_y
        
        fp_x
        fp_y
        
        a_P = 4;
        
        plot_flag = 1;
        
        non_CGG = 0;
    end
    
    properties (Constant)
        i_rrt = 5000;
        goal_diff = 10.0;
        goal_bias = 9;
        near_bias = 150;
        step_num = 3+1; % this is for spline interpolation parameter. 
        area_bias = 1.1; % for sampling area
        plot_step = 500;
        
        a_E = 1;
        P_max = 600 / 1000; % kJ/s
        PQ_max = 250;
    end
    
    methods
        function obj = RRTStarClass(map, uav_spec)
            obj.map = map;
            obj.uav_spec = uav_spec;
        end
        
        function tree = initRRT(obj, x0, y0, v0, xg, yg, a_P)

            obj.xg = xg;
            obj.yg = yg;
            obj.dist_2g = obj.area_bias * sqrt((xg-x0)^2+(yg-y0)^2)/2;
            obj.mid_x = (xg+x0)/2;
            obj.mid_y = (yg+y0)/2;
            
            obj.edge = obj.dist_2g*0.1;
            
            state_init = [x0 y0 v0 0 -1 0 0 0]'; 
            % x, y, v, cost, parent_node_id, E, P, t
            
            tree = [state_init]';
            
            obj.uav_spec.gen_cgg();
            x_range = linspace(0, obj.map.map_x, obj.map.map_x*obj.map.resolution);
            y_range = linspace(0, obj.map.map_y, obj.map.map_y*obj.map.resolution);
            [obj.fp_x obj.fp_y] = obj.uav_spec.get_fp(obj.map.map, x_range, y_range);
            
            if nargin > 6
                obj.a_P = a_P;
            end
        end
        
        function [tree, goal_id] = do_plan(obj, tree)
            goal_id = -1;
            cost_opt = inf;

            for i=1:1:obj.i_rrt
                
                % sample node
                [x_s y_s] = obj.get_rand_node(i);
                
                % get nearest tip state id
                nearest_id = obj.get_nearest_state(tree, x_s, y_s);
                % set new node between nearest state and sampled node
                [x_new y_new flag] = obj.set_new_node(nearest_id, tree, x_s, y_s);
                if flag == false
                    continue
                end
                nears_id = obj.get_near_states(tree, x_new, y_new);
                
                % get near states
                cost_new = inf;
                for j=1:1:length(nears_id)
                    near_id = nears_id(j);
                    % smooth edge
                    [S_x, S_y] = obj.smooth(tree, near_id, x_new, y_new);
                    
                    % optimize velocity
                    [state cost] = obj.opt_vel(S_x, S_y, tree(near_id, 3), tree(near_id, 1), tree(near_id, 2), tree(near_id, 4));
                    % cost check
                    if cost < cost_new
                        cost_new = cost;
                        state(4) = cost;
                        state(5) = near_id;
                        state_new = state;
                    end
                end
                
                % add state to tree
                if cost_new < cost_opt
                    tree = [tree; state_new'];
                else
                    continue;
                end
                
                % goal check
                if obj.check_goal(state_new(1), state_new(2)) && (state_new(4) < cost_opt)
                    goal_id = size(tree,1);
                    cost_opt = state_new(4);
                    if (obj.plot_flag == 1)
                        obj.plot_result(tree, goal_id);
                    end
                end
                
                % rewire
                id_new_state = size(tree,1);
                %nears_id = nears_id(nears_id~=id_new_state);
                for j=1:1:length(nears_id)
                    near_id = nears_id(j);
                    
                    % smooth edge
                    [S_x S_y] = obj.smooth(tree, id_new_state, tree(near_id, 1), tree(near_id, 2));
                    
                    % check two point boundary for velocity
                    dist = obj.calc_traj_length(S_x, S_y, state_new(1), state_new(2));
                    goal_flag = obj.check_goal(S_x, S_y);
                    % exclude initial state and vel=0 state out of goal area
                    %if (tree(near_id, 3) == 0) && (goal_flag == 0)
                    %    continue
                    %end
                    [cost E PQ t invalid_flag] = obj.calc_cost(S_x, S_y, state_new(3), tree(near_id, 3), dist, goal_flag);
                    cost = cost + state_new(4);
                    % cost check
                    if(cost < tree(near_id, 4)) && invalid_flag == 0
                        prev_cost = tree(near_id, 4);
                        
                        tree(near_id, 4) = cost;
                        tree(near_id, 5) = id_new_state;
                        tree(near_id, 6) = E;
                        tree(near_id, 7) = PQ;
                        tree(near_id, 8) = t;
                        
                        % recursively update the child state
                        tree = obj.update_child(tree, near_id, prev_cost);
                    end
                end
                
                % update goal reached trajectory
                for j=1:1:size(tree,1)
                    if obj.check_goal(tree(j,1), tree(j,2)) && (tree(j,4) < cost_opt)
                        goal_id = j;
                        cost_opt = tree(j, 4);
                    end
                end
                
                if(mod(i,obj.plot_step) == 0) && (obj.plot_flag == 1)
                    obj.plot_result(tree, goal_id);
                end
                
            end
        end
        
        function flag = check_angle(obj, rx0, ry0, rx1, ry1)
            % this function is possibly effective for convergence ?
            flag = 1;
        end

        function [x_s y_s] = get_rand_node(obj, sample_num)
                        
            x_s = random('Uniform',obj.mid_x-obj.dist_2g,obj.mid_x+obj.dist_2g);
            y_s = random('Uniform',obj.mid_y-obj.dist_2g,obj.mid_y+obj.dist_2g);
            
            if mod(sample_num, obj.goal_bias) == 0
                x_s = obj.xg;
                y_s = obj.yg;
            end
        end
        
        function nearest_id = get_nearest_state(obj, tree, x, y)
            dist=1234567;
            nears_id = [];

            for j=1:1:size(tree,1)
                tmp_dist = sqrt((tree(j, 1)-x)^2+(tree(j, 2)-y)^2);
                if(dist > tmp_dist) 
                    parent_id = tree(j,5);
                    if parent_id ~= -1
                        if obj.check_angle((x-tree(j, 1)), (y-tree(j, 2)), (x-tree(parent_id, 1)), (y-tree(parent_id, 2)))                
                            dist = tmp_dist;
                            nearest_id = j;
                        end
                    else
                        dist = tmp_dist;
                        nearest_id = j;
                    end
                end
            end
        end

        function [x_new y_new flag] = set_new_node(obj, near_id, tree, x_s, y_s)
            
            dist = sqrt((tree(near_id, 1)-x_s)^2+(tree(near_id, 2)-y_s)^2);
            % steer
            x_new = tree(near_id, 1) + obj.edge*(x_s - tree(near_id, 1))/dist;
            y_new = tree(near_id, 2) + obj.edge*(y_s - tree(near_id, 2))/dist;
            % check range 
            if((x_new < 0) || (x_new > obj.map.map_x) || (y_new < 0) || (y_new > obj.map.map_y))
                flag = false;
            elseif (isempty(find(tree(:,1)==x_new))==0) && (isempty(find(tree(:,2)==y_new))==0)
                flag = false;
            else
                flag = true;
            end
        end

        function nears_id = get_near_states(obj, tree, x, y)

            nears_id = [];           
            near_th = obj.near_bias * (log(length(tree))/length(tree))^0.5;

            for j=1:1:size(tree,1)
                tmp_dist = sqrt((tree(j, 1)-x)^2+(tree(j, 2)-y)^2);
                if(near_th > tmp_dist)
                    nears_id = [nears_id j];
                end
            end
        end

        function [S_x S_y] = smooth(obj, tree, near_id, x_new, y_new)
    
            parent_near_id = tree(near_id, 5);
            if parent_near_id == -1
                x_ctl = [tree(near_id, 1) x_new]; 
                y_ctl = [tree(near_id, 2) y_new];

                id_init = 1;
            else
                x_ctl = [tree(parent_near_id, 1) tree(near_id, 1) x_new];
                y_ctl = [tree(parent_near_id, 2) tree(near_id, 2) y_new];

                id_init = 2;
            end

            [S_x S_y] = obj.spline_2D(x_ctl, y_ctl, id_init, id_init+1, obj.step_num);

            S_x = S_x(2:end);
            S_y = S_y(2:end);

            % check if curve includes goal area
            [flag id] = obj.check_goal(S_x, S_y);
            if flag
                S_x = S_x(1:id);
                S_y = S_y(1:id);
                return
            end
        end

        function [S_x S_y] = spline_2D(obj, x_ctl, y_ctl, id_0, id_1, step_num)

            t = [0,cumsum(sqrt(diff(x_ctl).^2 + diff(y_ctl).^2))];
            x_seq = pchip(t,x_ctl);
            y_seq = pchip(t,y_ctl);
            tp = linspace(t(id_0),t(id_1),step_num);
            S_x = ppval(x_seq,tp);
            S_y = ppval(y_seq,tp);
        end
        
        function [state_opt cost_opt] = opt_vel(obj, S_x, S_y, v_cur, x_cur, y_cur, cost_cur)
    
            dist = obj.calc_traj_length(S_x, S_y, x_cur, y_cur);
            last_xy_id = size(S_x,2);
            
            [goal_flag id] = obj.check_goal(S_x, S_y);
            % optimize velocity
            if goal_flag
                v_opt = 0;
                [cost_tmp E_opt PQ_opt t_opt invalid_flag] = obj.calc_cost(S_x(1:id), S_y(1:id), v_cur, v_opt, dist, 1);
                cost_opt = cost_tmp + cost_cur;      
                last_xy_id = id;
            else
                vel_cand = [obj.uav_spec.v_min:obj.uav_spec.v_step:obj.uav_spec.v_max];
                v_opt = vel_cand(1);
                cost_opt = inf;
                E_opt = 0;
                PQ_opt = 0;
                t_opt = 0;

                for i=1:1:length(vel_cand)
                    v_tmp = vel_cand(i);

                    [cost_tmp E PQ t invalid_flag] = obj.calc_cost(S_x, S_y, v_cur, v_tmp, dist, 0);

                    if invalid_flag
                        continue
                    end

                    cost_tmp = cost_tmp + cost_cur;
                    if cost_tmp <= cost_opt
                        cost_opt = cost_tmp;
                        v_opt = v_tmp;
                        E_opt = E;
                        PQ_opt = PQ;
                        t_opt = t;
                    end
                end
            end

            state_opt = [[S_x(last_xy_id) S_y(last_xy_id)] v_opt -1 -1 E_opt PQ_opt t_opt ]';

        end

        function [flag id] = check_goal(obj, S_x, S_y)

            for i=1:1:length(S_x)
                dist = sqrt((S_x(i)-obj.xg)^2+(S_y(i)-obj.yg)^2);
                if dist < obj.goal_diff
                    flag = 1;
                    id = i;
                    return
                else
                    id = 0;
                    flag = 0;
                end
            end
        end

        function dist= calc_traj_length(obj, S_x, S_y, x_cur, y_cur)

            dist = sqrt((S_x(1)-x_cur)^2+(S_y(1)-y_cur)^2);
            for i=1:1:length(S_x)-1    
                delta_d = sqrt((S_x(i+1)-S_x(i))^2+(S_y(i+1)-S_y(i))^2);
                dist = dist + delta_d;
            end
        end
        
        function [cost E PQ t invalid_flag] = calc_cost(obj, S_x, S_y, v_cur, v_tmp, dist, goal_flag)
            
            % calc. energy consumption
            invalid_flag = 0;
            [E_acc_dec dist_acc_dec t_acc_dec] = obj.uav_spec.f_p_acc_dec(v_cur, v_tmp);
            if goal_flag == 1
                E = E_acc_dec;
                t = t_acc_dec;
            else
                if dist_acc_dec > dist
                       invalid_flag = 1;
                       E = 0;
                       PQ = 0;
                       t = 0;
                       cost = 0;
                       return
                end
                E = E_acc_dec + obj.uav_spec.f_p_v(v_tmp, dist-dist_acc_dec);
                t = t_acc_dec + (dist-dist_acc_dec)/v_tmp;        
            end
            E = E/1000.0;
            P = E / t;
            C_E = P / obj.P_max;
            if C_E > 1
                C_E = 1;
            end
            C_E = C_E * t;
            
            % calc. perception quality
            fim_list = [];
            for i=1:1:length(S_x)
                
                if obj.non_CGG
                    fp_x = [];
                    fp_y = []; 
                    for j=1:1:length(obj.fp_x)
                        if sqrt((obj.fp_x(j) - S_x(i))^2+(obj.fp_y(j) - S_y(i))^2) < obj.uav_spec.lidar_max_range
                            fp_x = [fp_x obj.fp_x(j)-S_x(i)];
                            fp_y = [fp_y obj.fp_y(j)-S_y(i)];
                        end             
                    end
                    fim_list = [fim_list obj.uav_spec.calc_fim(fp_x, fp_y, v_tmp)];
                else
                    [grid_x grid_y] = obj.uav_spec.check_grid(obj.fp_x - S_x(i), obj.fp_y - S_y(i));
                    fim_list = [fim_list obj.uav_spec.calc_fim(grid_x, grid_y, v_tmp)];
                end
            end
            PQ = mean(fim_list);
%             if length(fim_list)==0
%                 C_P = 1;
%             else
%                 C_P = 1 - PQ/obj.PQ_max;
%                 if C_P < 0
%                     C_P = 0;
%                 end
%             end        
            %cost = obj.alpha * C_P + (1.0 - obj.alpha) * C_E; 
            if PQ < 1
                C_P = 1;
            else
                C_P = 1/PQ;
            end
            C_P = C_P * t;
            cost = obj.a_E * C_E + obj.a_P * C_P;
        end

        function tree = update_child(obj, tree, parent_id, prev_cost_parent)

            childs_id = find(tree(:,5)==parent_id);
            if isempty(childs_id) == 0
                for i=1:1:length(childs_id)
                    child_id = childs_id(i);
                    cost_diff = tree(child_id, 4) - prev_cost_parent;
                    prev_cost_child = tree(child_id, 4);
                    tree(child_id, 4) = tree(parent_id, 4) + cost_diff;

                    tree = obj.update_child(tree, child_id, prev_cost_child);
                end  
            else
                tree = tree;
            end
        end

        function final_smoother(obj, tree, goal_id)

            figure(3)
            
            hold on;
            
            obj.traj_opt_x = [];        
            obj.traj_opt_y = [];
            
            v_list = [];

            parent = tree(goal_id, 5);
            child = goal_id;
            while 1
                if(parent == -1)
                    break
                end
                obj.traj_opt_x = [obj.traj_opt_x tree(child, 1)];
                obj.traj_opt_y = [obj.traj_opt_y tree(child, 2)];
                v_list = [v_list tree(child, 3)];

                child = parent;
                parent = tree(child, 5);
            end
            obj.traj_opt_x = [obj.traj_opt_x tree(child, 1)];
            obj.traj_opt_y = [obj.traj_opt_y tree(child, 2)];
            
            v_list = [v_list tree(child, 3)];
            d_interp = 10;
            [S_x S_y] = obj.spline_2D(obj.traj_opt_x, obj.traj_opt_y, 1, length(obj.traj_opt_x), d_interp*(length(v_list)-1));
            v_interp = [];
            for i=1:1:length(v_list)-1
                v_interp = [v_interp linspace(v_list(i), v_list(i+1), d_interp)];
            end
            %plot(S_x, S_y,'m-','LineWidth', 2)
            ax2 = axes;
            scatter(ax2, S_x, S_y, [], v_interp,'filled')
            axis equal
            colormap(ax2,'autumn')
            cb2 = colorbar(ax2,'Position',[0.055 0.1 0.05 0.815]);
            pbaspect([1 1 1])
            linkaxes([obj.map.ax_map,ax2])
            ax2.Visible = 'off';
            ax2.XTick = [];
            ax2.YTick = [];
            xlim([0 obj.map.map_x]);
            ylim([0 obj.map.map_y]);
            caxis([obj.uav_spec.v_min obj.uav_spec.v_max]);
        end
        
        function plot_result(obj, tree, goal_id)

            figure(2)

            hold on;

            delete(obj.tree_plot);
            obj.tree_plot = [];
            for j=1:1:size(tree,1)
                parent = tree(j, 5);
                if(parent~=-1)
                    [S_x S_y] = obj.smooth(tree, parent, tree(j, 1), tree(j, 2));
                    hold on;
                    obj.tree_plot = [obj.tree_plot plot( [tree(parent,1) S_x], [tree(parent,2) S_y],'k-','LineWidth', 2)];
                    hold on;
                end
            end

            if goal_id ~= -1
                delete(obj.traj_plot);
                obj.traj_plot = [];

                parent = tree(goal_id, 5);
                child = goal_id;
                while 1
                    if(parent == -1)
                        break
                    end
                    [S_x S_y] = obj.smooth(tree, parent, tree(child, 1), tree(child, 2));

                    obj.traj_plot = [obj.traj_plot plot( [tree(parent,1) S_x], [tree(parent,2) S_y],'r-','LineWidth', 2)];
                    hold on;

                    child = parent;
                    parent = tree(child, 5);
                end
            end

            set(gca, 'DataAspectRatio',[2 2 1]);
            set(0,'DefaultAxesLinewidth',2); 
            set(0,'DefaultLineLinewidth',2); 

        end

        function [cost E PQ total_t] = plot_v_E_P_result(obj, tree, goal_id)

            figure(4)
            v_list = [];
            t = [];
            E = 0; % total
            PQ = 0; % total
            if goal_id ~= -1
                parent = tree(goal_id, 5);
                child = goal_id;
                while 1
                    if(parent == -1)
                        break
                    end
                    v_list = [v_list tree(child, 3)];
                    t = [t tree(child, 8)];
                    E = E + tree(child, 6);
                    PQ = PQ + tree(child, 7) * t(end);
                    
                    child = parent;
                    parent = tree(child, 5);
                end
                t = cumsum(flip(t));
                plot([tree(child, 3) t], flip([v_list tree(child, 3)]),'k-','LineWidth', 2);
                hold on;
            end
            
            total_t = t(end);
            disp("Flight time = "+total_t+" [s]")
            disp("Energy consumption = "+E+" [kJ]")
            disp("Perception quality = "+PQ+" [-]")
            cost = tree(goal_id,4);
            disp("Cost = "+cost+" [-]")
            
            xlabel('Time [s]')
            ylabel('Velocity [m/s]')
            ylim([0 obj.uav_spec.v_max]);
            grid on;
            set(gca, 'DataAspectRatio',[2 2 1]);
            set(0,'DefaultAxesLinewidth',2); 
            set(0,'DefaultLineLinewidth',2); 

        end
    end
end
